#!/bin/bash

# シェルの動作を設定する
#   -e: コマンドがエラーになった時点でエラー終了する
#   -u: 未定義変数を参照した場合にエラー終了する
#   -x: 実行されるコマンドを引数を展開した上で表示する
set -eux

# インストールするパッケージのリスト
INSTALL_PACKAGES="\
  pdo_mysql \
  mbstring \
  mysqli
"

# パッケージのインストール時に、対話形式のユーザーインタフェースを抑制する
export DEBIAN_FRONTEND=noninteractive


# パッケージリストを取得する
apt-get update

# パッケージをインストールする
docker-php-ext-install ${INSTALL_PACKAGES}
